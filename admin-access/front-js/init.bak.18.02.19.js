/*
config file for the project
initialize all global variables in this file
author:joyesh@codomotive.com
contributors:
file created:02-11-2016(dd-mm-yyyy)
*/

//siteurl
//var ADDR="http://localhost/Green Tech-Admin-Panel-US";
var ADDR="http://ec2-54-67-100-17.us-west-1.compute.amazonaws.com/admin/Super-Admin";

//webservice

//var WEBSERVICE="http://192.168.2.89:9070";
var WEBSERVICE="https://pbnew.hashcashconsultants.com/Green Tech_Admin_US";


//set as debug mode
var DEBUG=1;

//page load identifier
var PL=0;

//work in progress identifier
var WIP=0;

//orderbook lowest quantity for adding ask and bid
var ORDERBOOKLOWESTQTY='00';

// currency icon for showining in the tables
var CURRENCY='<i class="fa fa-usd"></i>';
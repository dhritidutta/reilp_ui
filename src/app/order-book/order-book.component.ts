import { Component, OnInit, DoCheck } from "@angular/core";

import { TradesComponent } from "../trades/trades.component";

import * as $ from "jquery";

import { CoreDataService } from "../core-data.service";

import { Router } from "@angular/router";

import { DashboardComponent } from "../dashboard/dashboard.component";

import { HttpClient } from "@angular/common/http";

import { tokenKey } from "@angular/core/src/view";

import { ChartComponent } from "../../app/chart/chart.component";

import { Observable, BehaviorSubject } from 'rxjs';
import { map } from 'rxjs/operators';
export interface Todo {
  id: number | string;
  createdAt: number;
  value: string;
}

@Component({
  selector: "app-order-book",

  templateUrl: "./order-book.component.html",

  styleUrls: ["./order-book.component.css"]
})
export class OrderBookComponent implements OnInit {
  buyingAssetIssuer: string;

  sellingAssetIssuer: string;

  bidBody: string;

  askBody: string;

  orderBookUrl: string;

  count: any;

  lowprice: any;

  highprice;
  any = 0;

  chartlist: any = 0;

  ctpdata: any = 0;
  private _todos = new BehaviorSubject<Todo[]>([]);
  private baseUrl = 'https://mainnet.bitrump.com/';
  private dataStore: { todos: Todo[] } = { todos: [] };

  readonly todos = this._todos.asObservable();
  // session:any = localStorage.getItem('access_token');

  constructor(
    public data: CoreDataService,
    private route: Router,
    public dash: DashboardComponent,
    private http: HttpClient

  ) {
    switch (this.data.environment) {
      case "live":
         this.orderBookUrl = "https://mainnet.reilpexchange.com/";
        // this.orderBookUrl = "http://13.52.170.110:8000/"; //live
        break;

      case "dev":
        this.orderBookUrl =
          ""; //dev
        break;

      case "uat":
        this.orderBookUrl = ""
        //"http://ec2-52-8-41-112.us-west-1.compute.amazonaws.com:8000/"; //uat
        break;

      case "demo":
         this.orderBookUrl = "https://mainnet.reilpexchange.com/";
        // this.orderBookUrl = "http://13.52.170.110:8000/"; //demo
        break;
    }
    $("#orderbookBidBody").html(this.bidBody);
  }

  urlBid: any;

  urlAsk: any;

  selectedBuyingCryptoCurrencyName: string;

  selectedSellingCryptoCurrencyName: string;

  sellingAssetType: string;

  buyingAssetType: string;

  source11: any;

  source12: any;

  source2: any;

  token: any;

  marketTradeBodyHtml: any;

  // issuerLink: any =
  //   "./assets/appdata/assetIssuer." + this.data.environment + ".json";

  ngOnInit() {

    this.selectedSellingCryptoCurrencyName = this.data.selectedSellingCryptoCurrencyName;

    this.selectedBuyingCryptoCurrencyName = this.data.selectedBuyingCryptoCurrencyName;

    this.http
      .get<any>(
        this.data.CHARTSERVISE +
        "trendsTradeGraphFor24Hours/" +
        this.selectedBuyingCryptoCurrencyName +
        "/" +
        this.selectedSellingCryptoCurrencyName
      )

      .subscribe(value => {
        if (value != "") {
          this.chartlist = value[0];

         var randomNoForFlashArr = [];

          var arraylength = 2;

          this.ctpdata = this.chartlist.CTP;

          this.lowprice = this.chartlist.LOW_PRICE;

          this.highprice = this.chartlist.HIGH_PRICE;

          var randomNo = this.randomNoForOrderBook(0, arraylength);

          randomNoForFlashArr.push(randomNo);

          // console.log("DDDDDDDDDDDDD"+randomNo);

          for (var i = 0; i < arraylength; i++) {
            if (!$.inArray(i, randomNoForFlashArr)) {
              if (i == 0) {
                document.getElementById("pricered").style.display =
                  "inline-block";

                document.getElementById("pricegreen").style.display = "none";
              } else {
                document.getElementById("pricered").style.display = "none";

                document.getElementById("pricegreen").style.display =
                  "inline-block";
              }
            }
          }
        }
      });
    //this.loadAll();
  }
  changemode() {
    // console.log('++++++++++++++',this.data.changescreencolor);
    if (this.data.changescreencolor == true) {
      $(".bg_new_class")
        .removeClass("bg-dark")
        .css("background-color", "#fefefe");
      $(".sp-highlow").css("background-color", "#d3dddd");
      $(".sp-highlow").css("color", "Black");
      $(".border-col").css("border-color", "#d3dddd");
      $("th").css({ "background-color": "#d3dddd", color: "#273338" });
      $(".text-left").css("color", "black");
      $(".text-right").css("color", "black");
    } else {
      $(".bg_new_class")
        .removeClass("bg-dark")
        .css("background-color", "#16181a");
      $(".sp-highlow").css("background-color", "#273338");
      $(".sp-highlow").css("color", "yellow");
      $(".border-col").css("border-color", "#273338");
      $("th").css({ "background-color": "#273338", color: "#d3dddd" });
      $(".text-left")
        .css("color", "")
        .css("color", "white");
      $(".text-right").css("color", "white");
    }
  }
  ngDoCheck() {
    this.changemode();
    var randomNoForFlashArr = [];

    var arraylength = 2;

    var valcurrency = this.data.ctpdata;

    if (valcurrency == undefined) {
      this.ctpdata = this.chartlist.CTP;

      this.lowprice = this.chartlist.LOW_PRICE;

      this.highprice = this.chartlist.HIGH_PRICE;
      // console.log("data++++++++1", this.ctpdata, this.lowprice, this.highprice);
    } else {
      this.ctpdata = this.data.ctpdata;

      this.lowprice = this.data.lowprice;

      this.highprice = this.data.highprice;
      //console.log('data2++++++++',    this.ctpdata,this.lowprice, this.highprice)
    }

    var randomNo = this.randomNoForOrderBook(0, arraylength);

    randomNoForFlashArr.push(randomNo);

    for (var i = 0; i < arraylength; i++) {
      if (!$.inArray(i, randomNoForFlashArr)) {
        if (i == 0) {
          document.getElementById("pricered").style.display = "inline-block";

          document.getElementById("pricegreen").style.display = "none";
        } else {
          document.getElementById("pricered").style.display = "none";

          document.getElementById("pricegreen").style.display = "inline-block";
        }
      }
    }
  }

  //random function for flash class in server sent orderbook

  randomNoForOrderBook(minVal: any, maxVal: any): number {
    var minVal1: number = parseInt(minVal);

    var maxVal1: number = parseInt(maxVal);

    return Math.floor(Math.random() * (maxVal1 - minVal1 + 2) + minVal1);
  }

  //trade page setup
  serverSentEventForOrderbookBid() {
    this.sellingAssetType = "credit_alphanum12";
    this.buyingAssetType = "credit_alphanum12";
    var buyingissuer_code = localStorage.getItem('buying_crypto_asset');
    this.buyingAssetIssuer = this.data.selectedBuyingCryptoCurrencyissuer;
    this.sellingAssetIssuer = this.data.selectedSellingCryptoCurrencyissuer;
    var sellingissuer_code = localStorage.getItem('selling_crypto_asset');
    // var sellingissuer=localStorage.getItem('sellingissuer');
    var result: any;
    this.token = localStorage.getItem("access_token"); //Newly added by Arijit for Eventstream Management
    var url =
      this.orderBookUrl +
      "order_book?selling_asset_type=" +
      this.sellingAssetType +
      "&buying_asset_type=" +
      this.buyingAssetType +
      "&selling_asset_code=" +
      this.data.selectedSellingCryptoCurrencyName +
      "&selling_asset_issuer=" +
      this.sellingAssetIssuer +
      "&buying_asset_code=" +
      this.data.selectedBuyingCryptoCurrencyName +
      "&buying_asset_issuer=" +
      this.buyingAssetIssuer +
      "&order=desc";
    // console.log(url);
    if (this.token.length > 0) {
      //Newly added by Arijit for Eventstream Management
      this.source12 = new EventSource(url);
      this.source12.addEventListener("message", event => {
        result = JSON.parse(event.data);
        var bidHtml = "";
        var askHtml = "";
        if (result.bids.length != 0) {
          if (result.bids.length > 5) {//changed by sanu
            var bidLength: any = result.bids.length;
          } else {
            // var bidLength: any = 5; //changed by sanu
            var bidLength: any = result.bids.length;
          }
          var randomNoForFlashArr = [];
          var randomNo = 0;
          randomNoForFlashArr.push(randomNo);
          for (var i = 0; i < bidLength; i++) {
            var className = "";
            if (!$.inArray(i, randomNoForFlashArr)) {
              className = "bg-flash-green";
            }
            if (
              (result.bids[i].amount / result.bids[i].price).toFixed(4) != "0"
            ) {
              bidHtml += '<tr class="' + className + '">';

              bidHtml +=
                '<td class="text-left">' +
                (
                  parseFloat(result.bids[i].amount) / result.bids[i].price
                ).toFixed(4) +
                "</td>";

              if (this.data.selectedSellingCryptoCurrencyName == "usd") {
                bidHtml +=
                  '<td class="text-green text-right">' +
                  parseFloat(result.bids[i].price).toFixed(4) +
                  "</td>";
              } else {
                bidHtml +=
                  '<td class="text-green text-right">' +
                  parseFloat(result.bids[i].price).toFixed(8) +
                  "</td>";
              }

              bidHtml +=
                '<td class="text-right">' +
                (
                  (result.bids[i].amount / result.bids[i].price) *
                  result.bids[i].price
                ).toFixed(4) +
                "</td>";

              bidHtml += "</tr>";
            } else {
            }
          }
        } else {
          bidHtml += "<tr>";
          bidHtml += '<td class="text-white" colspan="2">No Data</td>';
          bidHtml += "</tr>";
        }
        // if(this.route.url=='/dashboard'){
        this.bidBody = bidHtml;
        $("#orderbookBidBody").html(this.bidBody);

        // }
      });
      //console.log(token);
    } else {

      //Newly added by Arijit for Eventstream Management
      this.source2.close();
    }
    // });
  }

  //Actual--dhriti
  serverSentEventForOrderbookAsk() {
    this.sellingAssetType = "credit_alphanum12";
    this.buyingAssetType = "credit_alphanum12";
    this.buyingAssetIssuer = this.data.selectedBuyingCryptoCurrencyissuer;
    this.sellingAssetIssuer = this.data.selectedSellingCryptoCurrencyissuer;
    var url =
      this.orderBookUrl +
      "order_book?selling_asset_type=" +
      this.sellingAssetType +
      "&buying_asset_type=" +
      this.buyingAssetType +
      "&selling_asset_code=" +
      this.data.selectedSellingCryptoCurrencyName +
      "&selling_asset_issuer=" +
      this.sellingAssetIssuer +
      "&buying_asset_code=" +
      this.data.selectedBuyingCryptoCurrencyName +
      "&buying_asset_issuer=" +
      this.buyingAssetIssuer +
      "&order=desc";
    if (this.token.length > 0) {
      this.source2 = new EventSource(url);
      this.urlAsk = url;
      var result: any = new Object();
      this.source2.onmessage = (event: MessageEvent) => {
        result = JSON.parse(event.data);
        var bidHtml = "";
        var askHtml = "";
        if (result.asks.length != 0) {
          if (result.asks.length > 5) { //changed by sanu
            var askLength = result.asks.length;
          } else {
            //var askLength: any = 5; //changed by sanu
            var askLength: any = result.asks.length;
          }
          var randomNoForFlashArr = [];
          // for(r=0;r<3;r++){
          var randomNo = this.randomNoForOrderBook(0, result.asks.length);
          randomNoForFlashArr.push(randomNo);
          //}
          //console.log(randomNoForFlashArr);

          for (var i=0; i<askLength; i++) { //changed by sanu
            // console.log(result.asks[i].amount);
            var className = "";
            if (!$.inArray(i, randomNoForFlashArr)) {
              var className = "bg-flash-red";
              //console.log(i+' in array');
            }
            if (parseFloat(result.asks[i].amount).toFixed(4) != "0.0000") {
              askHtml += '<tr class="' + className + '">';

              askHtml +=
                '<td class="text-left">' +
                parseFloat(result.asks[i].amount).toFixed(4) +
                "</td>";

              if (this.data.selectedSellingCryptoCurrencyName == "usd") {
                askHtml +=
                  '<td class="text-red text-right">' +
                  parseFloat(result.asks[i].price).toFixed(4) +
                  "</td>";
              } else {
                askHtml +=
                  '<td class="text-red text-right">' +
                  parseFloat(result.asks[i].price).toFixed(8) +
                  "</td>";
              }

              askHtml +=
                '<td class="text-right">' +
                (result.asks[i].amount * result.asks[i].price).toFixed(4) +
                "</td>";

              askHtml += "</tr>";
            } else {
            }
          }
        } else {
          askHtml += "<tr>";
          askHtml += '<td class="text-white" colspan="2">No Data</td>';
          askHtml += "</tr>";
        }
        this.askBody = askHtml;
        $("#orderbookAskBody").html(this.askBody);
      };
    } else {
      this.source2.close();
     // console.log("Session Expired");
    }
  }
  loadAll() {
    this.sellingAssetType = "credit_alphanum12";
    this.buyingAssetType = "credit_alphanum12";
    this.buyingAssetIssuer = this.data.selectedBuyingCryptoCurrencyissuer;
    this.sellingAssetIssuer = this.data.selectedSellingCryptoCurrencyissuer;
    var url =
      this.orderBookUrl +
      "order_book?selling_asset_type=" +
      this.sellingAssetType +
      "&buying_asset_type=" +
      this.buyingAssetType +
      "&selling_asset_code=" +
      this.data.selectedSellingCryptoCurrencyName +
      "&selling_asset_issuer=" +
      this.sellingAssetIssuer +
      "&buying_asset_code=" +
      this.data.selectedBuyingCryptoCurrencyName +
      "&buying_asset_issuer=" +
      this.buyingAssetIssuer +
      "&order=desc";
    this.http.get<Todo[]>(`${url}`).subscribe(data => {
      this.dataStore.todos = data;
      this._todos.next(Object.assign({}, this.dataStore).todos);
     // console.log('+++++++++++++++++', this.dataStore.todos);
      var holearray = this.dataStore.todos;
      var askarray = holearray['asks'];
      //console.log('+++++++++++++++++lenghthDhriti', askarray);
      var bidHtml = "";
      var askHtml = "";
      if (askarray.length != 0) {
        if (askarray.length > 5) { //changed by sanu
          var askLength = askarray.length;
        } else {
          //var askLength: any = 5; //changed by sanu
          var askLength: any = askarray.length;
        }
        var randomNoForFlashArr = [];
        // for(r=0;r<3;r++){
        var randomNo = this.randomNoForOrderBook(0, askarray.length);
        randomNoForFlashArr.push(randomNo);
        //}
        //console.log(randomNoForFlashArr);
// for (var i=askLength-1; i>=0; i--)
        for (var i = askLength - 1; i >= 0; i--) { //changed by sanu
          // console.log(result.asks[i].amount);
          var className = "";
          if (!$.inArray(i, randomNoForFlashArr)) {
            var className = "bg-flash-red";
            //console.log(i+' in array');
          }
          if (parseFloat(askarray[i].amount).toFixed(4) != "0.0000") {
            askHtml += '<tr class="' + className + '">';

            askHtml +=
              '<td class="text-left">' +
              parseFloat(askarray[i].amount).toFixed(4) +
              "</td>";

            if (this.data.selectedSellingCryptoCurrencyName == "usd") {
              askHtml +=
                '<td class="text-red text-right">' +
                parseFloat(askarray[i].price).toFixed(4) +
                "</td>";
            } else {
              askHtml +=
                '<td class="text-red text-right">' +
                parseFloat(askarray[i].price).toFixed(8) +
                "</td>";
            }

            askHtml +=
              '<td class="text-right">' +
              (askarray[i].amount * askarray[i].price).toFixed(4) +
              "</td>";

            askHtml += "</tr>";
          } else {
          }
        }
      } else {
        askHtml += "<tr>";
        askHtml += '<td class="text-white" colspan="2">No Data</td>';
        askHtml += "</tr>";
      }
      this.askBody = askHtml;
      $("#orderbookAskBody").html(this.askBody);
    //  console.log(' this.askBody++++++++Dhriti', this.askBody);

    }, error => console.log('Could not load todos.'));
  }
  loadAllbid() {
    this.sellingAssetType = "credit_alphanum12";
    this.buyingAssetType = "credit_alphanum12";
    this.buyingAssetIssuer = this.data.selectedBuyingCryptoCurrencyissuer;
    this.sellingAssetIssuer = this.data.selectedSellingCryptoCurrencyissuer;
    var url =
      this.orderBookUrl +
      "order_book?selling_asset_type=" +
      this.sellingAssetType +
      "&buying_asset_type=" +
      this.buyingAssetType +
      "&selling_asset_code=" +
      this.data.selectedSellingCryptoCurrencyName +
      "&selling_asset_issuer=" +
      this.sellingAssetIssuer +
      "&buying_asset_code=" +
      this.data.selectedBuyingCryptoCurrencyName +
      "&buying_asset_issuer=" +
      this.buyingAssetIssuer +
      "&order=desc";
    this.http.get<Todo[]>(`${url}`).subscribe(data => {
      this.dataStore.todos = data;
      this._todos.next(Object.assign({}, this.dataStore).todos);
      //console.log('+++++++++++++++++', this.dataStore.todos);
      var holearray = this.dataStore.todos;
      var bidarray = holearray['bids'];
     // console.log('+++++++++++++++++lenghthDhriti', bidarray);
      var bidHtml = "";
      var askHtml = "";
      if (bidarray.length != 0) {
        if (bidarray.length > 5) { //changed by sanu
          var bidLength = bidarray.length;
        } else {
          //var askLength: any = 5; //changed by sanu
          var bidLength: any = bidarray.length;
        }
        var randomNoForFlashArr = [];
        // for(r=0;r<3;r++){
        var randomNo = this.randomNoForOrderBook(0, bidarray.length);
        randomNoForFlashArr.push(randomNo);
        //}
        //console.log(randomNoForFlashArr);

        for (var i = 0; i < bidLength; i++) { //changed by sanu
          // console.log(result.asks[i].amount);
          var className = "";
          if (!$.inArray(i, randomNoForFlashArr)) {
            var className = "bg-flash-green";
            //console.log(i+' in array');
          }
          if (
            (bidarray[i].amount / bidarray[i].price).toFixed(4) != "0"
          ) {
            bidHtml += '<tr class="' + className + '">';

            bidHtml +=
              '<td class="text-left">' +
              (
                parseFloat(bidarray[i].amount) / bidarray[i].price
              ).toFixed(4) +
              "</td>";

            if (this.data.selectedSellingCryptoCurrencyName == "usd") {
              bidHtml +=
                '<td class="text-green text-right">' +
                parseFloat(bidarray[i].price).toFixed(4) +
                "</td>";
            } else {
              bidHtml +=
                '<td class="text-green text-right">' +
                parseFloat(bidarray[i].price).toFixed(8) +
                "</td>";
            }

            bidHtml +=
              '<td class="text-right">' +
              ((bidarray[i].amount / bidarray[i].price)*(bidarray[i].price)).toFixed(4) +
              "</td>";

              bidHtml += "</tr>";
          } else {
          }
        }
      } else {
        bidHtml += "<tr>";
        bidHtml += '<td class="text-white" colspan="2">No Data</td>';
        bidHtml += "</tr>";
      }
      this.bidBody = bidHtml;
      $("#orderbookBidBody").html(this.bidBody);
    //  console.log(' this.askBody++++++++Dhriti', this.bidBody);

    }, error => console.log('Could not load todos.'));
  }

  tradePageSetup() {
    if (
      this.data.selectedBuyingCryptoCurrencyName &&
      this.data.selectedBuyingCryptoCurrencyName
    ) {
     this.loadAll();
     this.loadAllbid();
    } else {
      // this.source12.close();
      // this.source2.close();
    }
  }
}

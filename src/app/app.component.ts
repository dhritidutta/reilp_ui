import { Component } from '@angular/core';
import { CoreDataService } from './core-data.service';
import * as $ from 'jquery';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { TradesComponent } from './trades/trades.component';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  constructor(private modalService: NgbModal, private data:CoreDataService, private http:HttpClient, private route:Router, private trade:TradesComponent){

    this.route.events.subscribe(()=>{
      if(this.route.url!='/dashboard'){
          // console.log('stop');
          clearInterval(this.trade.tradeInterval);
      }
  });

  this.data.idleLogout();

  }

  warn(warn) {
    this.modalService.open(warn);
  }

}
